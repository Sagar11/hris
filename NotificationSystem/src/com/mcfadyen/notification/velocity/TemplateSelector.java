package com.mcfadyen.notification.velocity;

import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class TemplateSelector {

	public String getTemplate(String templateId, Map<String, String> contextParams) {

		if (null != templateId) {
			Properties properties = new Properties();
			properties.put("resource.loader", "class");
			properties.put("class.resource.loader.class",
					"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

			VelocityEngine ve = new VelocityEngine();
			ve.init(properties);

			StringBuilder tempPath = new StringBuilder("templates/");
			tempPath.append(templateId);
			tempPath.append(".vm");
			String templatePath = tempPath.toString();

			Template template = ve.getTemplate(templatePath);

			VelocityContext context = new VelocityContext();
			for (Map.Entry<String, String> currentParam : contextParams.entrySet()) {
				String key = currentParam.getKey();
				String value = currentParam.getValue();
				context.put(key, value);
			}

			StringWriter out = new StringWriter();
			template.merge(context, out);
			String textTemplate = out.toString();
			return textTemplate;
		}
		return null;
	}
}
