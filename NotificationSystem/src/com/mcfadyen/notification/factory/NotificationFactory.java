package com.mcfadyen.notification.factory;

import com.mcfadyen.notification.provider.EmailNotification;
import com.mcfadyen.notification.provider.Notification;
import com.mcfadyen.notification.provider.SmsNotification;

public class NotificationFactory {

	public Notification getAdaptor(String adaptorType) {// enum
		if (adaptorType == null) {
			return null;
		} else if (adaptorType.equalsIgnoreCase("SMS")) {
			return new SmsNotification();
		} else if (adaptorType.equalsIgnoreCase("MAIL")) {
			return new EmailNotification();
		}
		return null;
	}
}
