package com.mcfadyen.notification.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Arrays;

import javax.activation.DataSource;

/**
 * @author pgls
 * POJO to support binary data source
 *
 */
public class AttachmentDataSource implements DataSource, Serializable {
	private static final long serialVersionUID = -4625559171335137917L;
	private String mContentType;
	private String mFilename;
	private byte[] mDataSourceBytes;

	public AttachmentDataSource(byte[] paramArrayOfByte, String contentType,
			String fileName) {
		this.mContentType = contentType;
		this.mDataSourceBytes = Arrays.copyOf(paramArrayOfByte, paramArrayOfByte.length);
		this.mFilename = fileName;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		throw new IOException("OutputStream not supported.");
	}

	@Override
	public String getContentType() {
		return this.mContentType;
	}
	
	@Override
	public String getName() {
		return this.mFilename;
	}
	
	@Override
	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(this.mDataSourceBytes);
	}

}
