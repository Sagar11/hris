package com.mcfadyen.notification.tools;

import java.util.List;
import java.util.Map;

import com.mcfadyen.notification.provider.MailContent;
import com.mcfadyen.notification.provider.SmsContent;

public class SetData {
	static final String TO = "to";
	static final String FROM = "from";
	static final String SUBJECT = "subject";
	static final String ATTACHMENT = "attachment";
	static final String ATTACHMENT_NAME = "attachmentName";
	static final String ATTACHMENT_TYPE = "attachmentType";
	static final String MESSAGE = "message";
	static final String TEXT_MESSAGE = "textMessage";
	static final String NUMBER_LIST = "numberList";
	static final String SENDER_ID = "senderId";
	static final String TEMPLATE_ID = "templateId";
	static final String CONTEXT_PARAM = "contextParam";

	@SuppressWarnings("unchecked")
	public static void setFields(Map<String, Object> fieldData) {
		for (Map.Entry<String, Object> entry : fieldData.entrySet()) {

			String key = entry.getKey();

			Object value = entry.getValue();

			switch (key) {
			case TO:
				MailContent.setTo((String[]) value);
				break;
			case FROM:
				MailContent.setFrom((String) value);
				break;
			case SUBJECT:
				MailContent.setSubject((String) value);
				break;
			case ATTACHMENT:
				MailContent.setAttachment((byte[]) value);
				break;
			case ATTACHMENT_NAME:
				MailContent.setAttachmentName((String) value);
				break;
			case ATTACHMENT_TYPE:
				MailContent.setAttachmentType((String) value);
				break;
			case MESSAGE:
				MailContent.setMessage((String) value);
				break;
			case TEXT_MESSAGE:
				SmsContent.setMessage((String) value);
				break;
			case NUMBER_LIST:
				SmsContent.setNumberList((String[]) value);
				break;
			case SENDER_ID:
				SmsContent.setSenderId((String) value);
				break;
			
			case TEMPLATE_ID:
				MailContent.setTemplateId((String) value);
				SmsContent.setTemplateId((String) value);
				break;
			
			case CONTEXT_PARAM:
				MailContent.setContextParam((List<Map<String, String>>) value);
				SmsContent.setContextParam((List<Map<String, String>>) value);
				break;
			}
				

		}
	}
}
