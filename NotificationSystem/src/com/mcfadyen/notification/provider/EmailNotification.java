package com.mcfadyen.notification.provider;

import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.mcfadyen.notification.utils.AttachmentDataSource;
import com.mcfadyen.notification.velocity.TemplateSelector;

public class EmailNotification implements Notification {
	static Session session;
	static Properties properties;
	static final String HOST = "172.31.11.19";
	static final String CONTEXT_TYPE = "text/html";

	public boolean send() {
		properties = System.getProperties();
		properties.setProperty("mail.smtp.host", HOST);
		properties.setProperty("port", "25");
		session = Session.getDefaultInstance(properties);

		for (String to : MailContent.getTo()) {
			try {
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(MailContent.getFrom()));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
				message.setSubject(MailContent.getSubject());
				BodyPart messageBodyPart = new MimeBodyPart();
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);
				//Templating
				int i = 0;
				if(null != MailContent.getTemplateId()){
					TemplateSelector ts = new TemplateSelector();
					List<Map<String, String>> contextParams = SmsContent.getContextParam();
					Map<String, String> currentParam = contextParams.get(i);
					messageBodyPart.setContent(ts.getTemplate(MailContent.getTemplateId(), currentParam), CONTEXT_TYPE);
					i++;
				}
				else{
					messageBodyPart.setText(MailContent.getMessage());
				}
				// attachment code
				if (null != MailContent.getAttachmentName() && null != MailContent.getAttachment()
						&& null != MailContent.getAttachmentType()) {
					BodyPart attachmentBodyPart = new MimeBodyPart();
					String fileName = MailContent.getAttachmentName();//Needs file with extension
					String fileType = MailContent.getAttachmentType();

					byte[] decodedAttachment = Base64.getDecoder().decode(MailContent.getAttachment());
					AttachmentDataSource source = new AttachmentDataSource(decodedAttachment, fileType, fileName);
					attachmentBodyPart.setDataHandler(new DataHandler(source));
					attachmentBodyPart.setFileName(fileName);
					multipart.addBodyPart(attachmentBodyPart);
				}
				
				message.setContent(multipart);
				Transport.send(message);
				System.out.println("Sent message");
				
			} catch (MessagingException mex) {
				System.out.println("Error while sending mail");
				mex.printStackTrace();
				return false;
			}
			
		}return true;
	}

}