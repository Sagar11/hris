package com.mcfadyen.notification.provider;

import java.util.List;
import java.util.Map;

public class SmsContent {
	private static String[] numberList;
	private static String message;
	private static String senderId;
	private static String templateId;
	private static List<Map<String,String>> contextParam;

	public static String[] getNumberList() {
		return numberList;
	}

	public static void setNumberList(String[] numberList) {
		SmsContent.numberList = numberList;
	}

	public static String getMessage() {
		return message;
	}

	public static void setMessage(String message) {
		SmsContent.message = message;
	}

	public static String getSenderId() {
		return senderId;
	}

	public static void setSenderId(String senderId) {
		SmsContent.senderId = senderId;
	}

	public static List<Map<String,String>> getContextParam() {
		return contextParam;
	}

	public static void setContextParam(List<Map<String, String>> value) {
		SmsContent.contextParam = value;
	}

	public static String getTemplateId() {
		return templateId;
	}

	public static void setTemplateId(String templateId) {
		SmsContent.templateId = templateId;
	}
}
