package com.mcfadyen.notification.provider;

import java.util.List;
import java.util.Map;

public class MailContent {

	private static String[] to;
	private static String from;
	private static String subject;
	private static byte[] attachment;
	private static String attachmentName;
	private static String attachmentType;
	private static String message;
	private static String templateId;
	private static List<Map<String,String>> contextParam;

	public static String[] getTo() {
		return to;
	}

	public static void setTo(String[] to) {
		MailContent.to = to;
	}

	public static String getFrom() {
		return from;
	}

	public static void setFrom(String from) {
		MailContent.from = from;
	}

	public static String getSubject() {
		return subject;
	}

	public static void setSubject(String subject) {
		MailContent.subject = subject;
	}

	public static byte[] getAttachment() {
		return attachment;
	}

	public static void setAttachment(byte[] value) {
		MailContent.attachment = value;
	}

	public static String getMessage() {
		return message;
	}

	public static void setMessage(String message) {
		MailContent.message = message;
	}

	public static String getAttachmentName() {
		return attachmentName;
	}

	public static void setAttachmentName(String attachmentName) {
		MailContent.attachmentName = attachmentName;
	}

	public static String getAttachmentType() {
		return attachmentType;
	}

	public static void setAttachmentType(String attachmentType) {
		MailContent.attachmentType = attachmentType;
	}

	public static List<Map<String,String>> getContextParam() {
		return contextParam;
	}

	public static void setContextParam(List<Map<String, String>> contextParam) {
		MailContent.contextParam = contextParam;
	}

	public static String getTemplateId() {
		return templateId;
	}

	public static void setTemplateId(String templateId) {
		MailContent.templateId = templateId;
	}
}
