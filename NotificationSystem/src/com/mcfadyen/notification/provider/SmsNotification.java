package com.mcfadyen.notification.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import com.mcfadyen.notification.velocity.TemplateSelector;

public class SmsNotification implements Notification {
	// Replace with your username
	static final String USER = "aantony1";

	// Replace with your API KEY (We have sent API KEY on activation email, also
	// available on panel)
	static final String API_KEY = "FgbY92Hb92yxCLWssPJI";

	// Replace with the destination mobile Number to which you want to send sms

	// Replace if you have your own Sender ID, else donot change

	// Replace with your Message content

	// For Plain Text, use "txt" ; for Unicode symbols or regional Languages
	static final String TYPE = "uni";

	@SuppressWarnings("deprecation")
	public boolean send() {
		// Prepare Url
		URLConnection myURLConnection = null;
		URL myURL = null;
		BufferedReader reader = null;

		// encoding message
		// String encoded_message = URLEncoder.encode(SmsContent.getMessage());

		// Send SMS API
		String mainUrl = "http://smshorizon.co.in/api/sendsms.php?";
		int i = 0;
		for (String mobile : SmsContent.getNumberList()) {
			String encodedMessage;
			if (null != SmsContent.getTemplateId()) {
				TemplateSelector ts = new TemplateSelector();
				List<Map<String, String>> contextParams = SmsContent.getContextParam();
				Map<String, String> currentParam = contextParams.get(i);
				encodedMessage = URLEncoder.encode(ts.getTemplate(SmsContent.getTemplateId(), currentParam));
				i++;
			} else {
				encodedMessage = URLEncoder.encode(SmsContent.getMessage());
			}

			// Prepare parameter string
			StringBuilder sbPostData = new StringBuilder(mainUrl);
			sbPostData.append("user=" + USER);
			sbPostData.append("&apikey=" + API_KEY);
			sbPostData.append("&message=" + encodedMessage);
			sbPostData.append("&mobile=" + mobile);
			sbPostData.append("&senderid=" + SmsContent.getSenderId());
			sbPostData.append("&type=" + TYPE);

			// final string
			mainUrl = sbPostData.toString();
			try {
				// prepare connection
				myURL = new URL(mainUrl);
				myURLConnection = myURL.openConnection();
				myURLConnection.connect();
				reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
				// reading response
				String response;
				while ((response = reader.readLine()) != null)
					// print response
					System.out.println(response);

				// finally close connection
				reader.close();

			} catch (IOException e) {
				System.out.println("IO exception");
				return false;
			}

		}
		return true;

	}

}
