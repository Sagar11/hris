package com.mcfadyen.notification.provider;

public interface Notification {

	public boolean send();
}
