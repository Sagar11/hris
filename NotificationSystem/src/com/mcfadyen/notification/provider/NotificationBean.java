package com.mcfadyen.notification.provider;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class NotificationBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String type;
	private static String[] to;
	private static String from;
	private static String subject;
	private static String message;
	private static String templateId;
	private static List<Map<String, String>> contextParam;
	private static byte[] attachment;
	private static String attachmentName;
	private static String attachmentType;

	public static String[] getTo() {
		return to;
	}

	public static void setTo(String[] to) {
		NotificationBean.to = to;
	}

	public static String getFrom() {
		return from;
	}

	public static void setFrom(String from) {
		NotificationBean.from = from;
	}

	public static String getSubject() {
		return subject;
	}

	public static void setSubject(String subject) {
		NotificationBean.subject = subject;
	}

	public static String getMessage() {
		return message;
	}

	public static void setMessage(String message) {
		NotificationBean.message = message;
	}

	public static String getTemplateId() {
		return templateId;
	}

	public static void setTemplateId(String templateId) {
		NotificationBean.templateId = templateId;
	}

	public static List<Map<String, String>> getContextParam() {
		return contextParam;
	}

	public static void setContextParam(List<Map<String, String>> contextParam) {
		NotificationBean.contextParam = contextParam;
	}

	public static byte[] getAttachment() {
		return attachment;
	}

	public static void setAttachment(byte[] attachment) {
		NotificationBean.attachment = attachment;
	}

	public static String getAttachmentName() {
		return attachmentName;
	}

	public static void setAttachmentName(String attachmentName) {
		NotificationBean.attachmentName = attachmentName;
	}

	public static String getAttachmentType() {
		return attachmentType;
	}

	public static void setAttachmentType(String attachmentType) {
		NotificationBean.attachmentType = attachmentType;
	}

	public static String getType() {
		return type;
	}

	public static void setType(String type) {
		NotificationBean.type = type;
	}

}
