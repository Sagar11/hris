package com.mcfadyen.notification.manager;

import java.util.Map;

import com.mcfadyen.notification.factory.NotificationFactory;
import com.mcfadyen.notification.provider.Notification;
import com.mcfadyen.notification.tools.SetData;

public class NotificationManager {

	public boolean send(Map<String, Object> sendInfo, String adaptorType) {
		if (null != adaptorType) {
			SetData.setFields(sendInfo);
			NotificationFactory factory = new NotificationFactory();
			Notification adaptor = factory.getAdaptor(adaptorType);

			if (adaptor.send())
				return true;
			else {
				return false;
			}
		}
		return false;
	}

}
