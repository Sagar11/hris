package com.mcfadyen.notification.invoke;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.apache.commons.io.FileUtils;

import com.mcfadyen.notification.manager.NotificationManager;

@Path("/notification")
public class CreateCall {

	@GET
	@Path("/mail")
	public String sendMail() {

		NotificationManager nm = new NotificationManager();
		Map<String, Object> sendInfo = new HashMap<>();

		Properties prop = new Properties();

		try {

			ClassLoader classLoader = this.getClass().getClassLoader();
			// prop.load(CreateCall.class.getResourceAsStream("config/mail-settings.properties"));
			prop.load(classLoader.getResourceAsStream("/config/mail-settings.properties"));

			String[] to = prop.getProperty("to").split(";");
			String[] tempContext = prop.getProperty("fname").split(";");
			List<Map<String, String>> maps = new ArrayList<Map<String, String>>();
			for (String tContext : tempContext) {
				String temp = tContext;
				Map<String, String> mp = new HashMap<String, String>();
				mp.put("fname", temp);
				maps.add(mp);
			}

			String templateId = prop.getProperty("templateId");
			String from = prop.getProperty("from");
			String subject = prop.getProperty("subject");
			String fileName = prop.getProperty("fileName");
			String fileType = prop.getProperty("fileType");
			String filePath = prop.getProperty("filePath");

			File file = new File(filePath);
			byte[] byteArray, byteArrayEncoded = null;

			try {
				byteArray = FileUtils.readFileToByteArray(file);
				byteArrayEncoded = Base64.getEncoder().encode(byteArray);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			sendInfo.put("to", to);
			sendInfo.put("from", from);
			sendInfo.put("subject", subject);
			sendInfo.put("templateId", templateId);
			sendInfo.put("contextParam", maps);
			// sendInfo.put("message", msg);
			sendInfo.put("attachment", byteArrayEncoded);
			sendInfo.put("attachmentName", fileName);
			sendInfo.put("attachmentType", fileType);
			if (nm.send(sendInfo, "mail")) {

				return "<html> <head> <title> Success </title> </head> <body> <h1> Mail Sent </h1> </body> </html>";
			} else
				return "<html> <head> <title> Failed </title> </head> <body> <h1> Mail Failed </h1> </body> </html>";

		} catch (IOException ex) {
			ex.printStackTrace();

		}
		return "<html> <head> <title> Failed </title> </head> <body> <h1> Mail Failed </h1> </body> </html>";
	}

	@GET
	@Path("/sms")
	public String sendSms() {

		NotificationManager nm = new NotificationManager();

		Properties prop = new Properties();
		// InputStream input = null;

		try {

			// input = new FileInputStream("/config/sms-settings.properties");
			//
			// // load a properties file
			// prop.load(input);

			ClassLoader classLoader = this.getClass().getClassLoader();
			// prop.load(CreateCall.class.getResourceAsStream("config/mail-settings.properties"));
			prop.load(classLoader.getResourceAsStream("/config/sms-settings.properties"));

			String[] numberList = prop.getProperty("numberList").split(";");
			String templateId = prop.getProperty("templateId");
			String senderId = prop.getProperty("senderId");

			String[] tempContext = prop.getProperty("fname").split(";");

			List<Map<String, String>> maps = new ArrayList<Map<String, String>>();

			for (String tContext : tempContext) {
				String temp = tContext;
				Map<String, String> mp = new HashMap<String, String>();
				mp.put("fname", temp);
				maps.add(mp);
			}

			Map<String, Object> sendInfo = new HashMap<>();

			sendInfo.put("numberList", numberList);
			sendInfo.put("templateId", templateId);
			sendInfo.put("contextParam", maps);
			sendInfo.put("senderId", senderId);

			if (nm.send(sendInfo, "sms")) {

				return "<html> <head> <title> Success </title> </head> <body> <h1> Sms Sent</h1> </body> </html>";
			} else
				return "<html> <head> <title> Failed </title> </head> <body> <h1> Sms Failed </h1> </body> </html>";

		} catch (IOException ex) {
			ex.printStackTrace();

		}
		return "<html> <head> <title> Failed </title> </head> <body> <h1> Sms Failed </h1> </body> </html>";
	}

	@GET
	@Path("/notify")
	public String call() {

		CreateCall callBoth = new CreateCall();
		String result = callBoth.sendMail();
		//result += callBoth.sendSms();
		result += "<html><body><a href=\"http://localhost/Project_HRIS/profile-general-HR%20perspective.html\">Back to home</a></body></html>";
		return result;
	}
}
