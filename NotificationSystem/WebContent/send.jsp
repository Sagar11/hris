<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/send.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<title>Send Notification</title>
</head>
<body>
<jsp:useBean id="obj" class=com.mcfadyen.notification.provider.NotificationBean></jsp:useBean>
	<header class="page-header">
		<h1>Notification Manager</h1>
	</header>
	<nav class="navbar-default">
		<a href="send.html" class="btn active">Send Notification</a> <a
			href="track.html" class="btn ">Track Notification</a>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="#"><span class="glyphicon glyphicon-log-out"></span>
					Log Out</a></li>
			<span class="col-md-2"></span>
		</ul>

	</nav>

	<div class="container">
		<div class="form-group">
			<div class="row">
				<form method="post" name="sendForm" class="form-horizontal">
					<div class="col-md-4">
						<br> <label class="control-label">Provider:</label> <select
							class="btn btn-default form-control" name="provider"
							id="provider" onchange="enableDisable()">
							<option selected value="email">e-Mail</option>
							<option value="sms">SMS</option>
						</select> <br>
						<br> <label class="control-label">User Group:</label> <select
							class="btn btn-default form-control" name="userGroup">
							<option selected value="blr">BLR</option>
							<option value="livelo">Livelo</option>
							<option value="other">Other</option>
						</select> <br>
					</div>
					<div id="sms-div" class="col-md-4">
						<br>
						<label class="control-label">Sender id:</label> <select
							class="btn btn-default form-control" name="senderid">
							<option value="vibranz">Vibranz</option>
							<option value="it">IT Support</option>
							<option value="finance">Finance</option>
						</select> <br>
						<label class="control-label">Message:</label>
						<textarea name="msg" class="form-control" rows="5" id="msg"></textarea>
						<p id="warning" class="text-danger" hidden>Maximum 160
							Characters</p>
					</div>
					<div id="email-div" class="col-md-4">
						<br> <label class="control-label">Template:</label> <select
							class="btn btn-default form-control" name="template"
							id="template" onchange="useTemplate()">
							<option selected></option>
							<option value="birthday">Birthday</option>
							<option value="holiday">Holiday</option>
							<option value="none">None</option>
						</select> <br> <br> <label class="control-label">Subject:</label>
						<input type="text" name="subject" id="subject"
							class="form-control"> <br> <label
							class="control-label">Attachment:</label> <input type="file"
							name="attachment" id="attachment" class="btn btn-default">
					</div>
			</div>
			<input type="submit" value="filter" class="btn btn-success"> <br>
			</form>
		</div>
	</div>
	</div>
	<footer>McFadyen Solutions</footer>
</body>
<script type="text/javascript" src="js/send.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</html>